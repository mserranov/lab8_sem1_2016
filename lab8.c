/*Mariangel Mujica Torrealba - Matías Serrano Vera*/

#include <stdio.h>
#include <stdlib.h>

#define N 8
//por ahora lo dejo como un array limitado, la idea es sistematizarlo
int visitados[N]= {0,0,0,0,0,0,0,0};

//para anchura
int cola[100];
int total = 0;
int primero = 0;
int ultimo = 0;

//Kruskal y Prim
int i,j,k,a,b,u,v,n,ne=1;
int min,costomin=0,padre[9];
int filas = 8;
int columnas = 8;
int n = 8;
int i=0, j;

void AlgoritmoPrim(int **adyacencia){
	int i, j, inicio, final;
	int k, nr[10], aux, mincost = 0, arbol[10][3];

	aux = adyacencia[0][0];

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			if (aux >= adyacencia[i][j]) {
				aux = adyacencia[i][j];
				inicio = i;
				final = j;
			}
		}	
	}
	arbol[0][0] = inicio;
	arbol[0][1] = final;
	arbol[0][2] = aux;
	mincost = aux;

	for (i = 0; i<n; i++) {
		if (adyacencia[i][inicio] < adyacencia[i][final])
			nr[i] = inicio;
		else
			nr[i] = final;
	}
	nr[inicio] = 100;
	nr[final] = 100;
	
	aux = 99;
	for (i = 1; i<n - 1; i++) {
		for (j = 0; j<n; j++) {
			if (nr[j] != 100 && adyacencia[j][nr[j]] < aux) {
				aux = adyacencia[j][nr[j]];
				k = j;
			}
		}

		arbol[i][0] = k;
		arbol[i][1] = nr[k];
		arbol[i][2] = adyacencia[k][nr[k]];
		mincost = mincost + adyacencia[k][nr[k]];
		nr[k] = 100;

		for (j = 0; j<n; j++) {
			if (nr[j] != 100 && adyacencia[j][nr[j]] > adyacencia[j][k])
				nr[j] = k;
		}
		aux = 99;
	}
	printf("El arbol de expansion mimina segun Prim es:\n");
	for (i = 0; i < n - 1; i++) {
		for (j = 0; j < 3; j++)
			printf("%3d", arbol[i][j]);
			printf("\n");
	}

	printf("\nCosto minimo segun Prim: %d", mincost);
	}


char NodoLetra(int nodo){
	char nodoletra;
	switch(nodo){
		case 0:
			nodoletra = 'A';
		break;
		case 1:
			nodoletra = 'B';
		break;
		case 2:
			nodoletra = 'C';
		break;
		case 3:
			nodoletra = 'D';
		break;
		case 4:
			nodoletra = 'E';
		break;
		case 5:
			nodoletra = 'F';
		break;
		case 6:
			nodoletra = 'G';
		break;
		case 7:
			nodoletra = 'H';
		break;
	}
	return nodoletra;
}

void RecorridoProfundidad(int **matriz, int V){
	int W;
	char res;
	visitados[V] = 1;
	res = NodoLetra(V);
	printf("%c\n", res);
	for(W = 0;W < N; W++){ 
		if (matriz[V][W] != 0){ 
			if (visitados[W] == 0){ 
				RecorridoProfundidad(matriz, W);
			}
		}
	}
}

void RecorridoAnchura(int **matriz, int V){
	int visitados[N]= {0,0,0,0,0,0,0,0};
	int Z;
	int W;
	visitados[V] = 1;
	printf("%c\n", NodoLetra(V));
	total++;
	primero++;
	ultimo++;
	cola[ultimo] = V;
	while(total > 0){
		Z = cola[primero];
		primero++;
		total--;
		for(W = 0; W<N; W++){
			if(matriz[Z][W] != 0){
				if(visitados[W] == 0){
					visitados[W] = 1;
					printf("%c\n", NodoLetra(W));
					total++;
					ultimo++;
					cola[ultimo] = W;
				}
			}
		}
	}
}

void MostrarDatos(int **matriz,int filas,int columnas){
	int i,j;
	printf("\nLa matriz almacenada es:\n");
    for(i=0; i<filas; i++){
		for(j=0; j<columnas; j++){
			printf("%2d ", matriz[i][j]);
		}
		printf("\n");
	}
}


int Busqueda(int i){
	while(padre[i]){
		i=padre[i];
	}
	return i;
}

int uni(int i,int j){
	if(i!=j){
		padre[j] = i;
		return 1;
	}
	return 0;
}

void AlgoritmoKruskal(int **adyacencia){
	for(i=0; i<filas; i++){
		for(j=0; j<columnas; j++){
			if(adyacencia[i][j] == 0){
				adyacencia[i][j]=999;
			}
		}
	}
	
	//printf("\nLos arboles de expansion minimos de expansion y sus costos son:\n");
	while(ne < filas){
		for(i=0,min=999;i<filas;i++){
			for(j=0;j < filas;j++){
				if(adyacencia[i][j] < min){
					min=adyacencia[i][j];
					a=u=i;
					b=v=j;
				}
			}
		}
		u=Busqueda(u);
		v=Busqueda(v);
		if(uni(u,v)){
			printf("%d -> (%d,%d) =%d\n",ne++,a,b,min);
			costomin =  min + costomin;
		}
		adyacencia[a][b]=adyacencia[b][a]=999;
	}
	printf("\nCosto minimo segun Kruskal = %d\n",costomin);
}

void BuscarNodo(int **adyacencia){
	int nodo;
	int columnas = 8;
	int j;
	
	printf("Ingrese un nodo entre 1 y %d nodo para analizar el vecino:\n", columnas);
	scanf("%d", &nodo);
	while(nodo <= 0 || nodo > columnas){
		printf("Error! el nodo debe estar entre 1 y %d nodo\nReingrese:\n", columnas);
		scanf("%d", &nodo);
	}
	
	printf("El nodo %d es el nodo %c.\n", nodo, NodoLetra(nodo-1));
	
	for(j=0; j<columnas; j++){
		if(adyacencia[nodo-1][j] != 0){
			printf("Tiene como vecino al nodo %c", NodoLetra(j));
			printf(", y su peso es %d\n", adyacencia[nodo-1][j]);
		}	
	}	
}

int **matrizAdyacencia(){
	FILE *fichero;
	int **adyacencia;
	int filas = 8;
	int columnas = 8;
	int *m;
	int i=0;
	adyacencia = malloc(filas * sizeof(int*));
	fichero = fopen("adyacencia.dat","r");        
	
	if (!fichero)
		printf("Error de lectura\n");
	else{
		m = malloc(columnas * sizeof(int));
		while(i != columnas){
			fscanf(fichero,"%d %d %d %d %d %d %d %d", &m[0],&m[1],&m[2],&m[3],&m[4],&m[5],&m[6],&m[7]);
			adyacencia[i] = m;
			i++;	
			if(i != columnas)
				m = malloc(columnas * sizeof(int));
		}
		MostrarDatos(adyacencia, filas, columnas);
	}
	fclose(fichero);
	printf("\n\n");
	
	return adyacencia;
}

void matrizIncidencia(){
	FILE *fp;
	int **incidencia;
	int filas = 8;
	int columnas = 15;
	int *m;
	int i=0;
	incidencia = malloc(filas * sizeof(int*));	
	
	fp = fopen("incidencia.dat","r");   
	 
	if (!fp)
		printf("Error de lectura\n");
	else{
		m = malloc(columnas * sizeof(int));
		while(i != filas){
			fscanf(fp,"%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d", &m[0],&m[1],&m[2],&m[3],&m[4],&m[5],&m[6],&m[7],&m[8],&m[9],&m[10],&m[11],&m[12],&m[13],&m[14]);
			incidencia[i] = m;
			i++;
			if(i != columnas)
				m = malloc(columnas * sizeof(int));
		}
		MostrarDatos(incidencia, filas, columnas);
	}
	fclose (fp);
}

int main(){
	int **m;
	m = matrizAdyacencia();
	matrizIncidencia();
	BuscarNodo(m);
	printf("\nEl recorrido DFS (busqueda en profundidad) es:\n");
	RecorridoProfundidad(m, 1);
	printf("\nEl recorrido DFS (busqueda en profundidad) es:\n");
	RecorridoAnchura(m,0);
	printf("\nEl arbol de cobertura mínimo por Kruskal es:\n");
	AlgoritmoKruskal(m);
	printf("\nAplicando Prim:\n");
	AlgoritmoPrim(m);
	return 0;
	}
