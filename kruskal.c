#include <stdio.h>
#include <stdlib.h>
int i,j,k,a,b,u,v,n,ne=1;
int min,costomin=0,padre[9];
FILE *fichero;
int **adyacencia;
int filas = 8;
int columnas = 8;
int *m;

void MostrarDatos(int **matriz,int filas,int columnas){
	int i,j;
	printf("\nLa matriz almacenada es:\n");
    for(i=0; i<filas; i++){
		for(j=0; j<columnas; j++){
			printf("%2d ", matriz[i][j]);
		}
		printf("\n");
	}
}

int Busqueda(int i){
	while(padre[i]){
		i=padre[i];
	}
	return i;
}

int uni(int i,int j){
	if(i!=j){
		padre[j] = i;
		return 1;
	}
	return 0;
}

int main(){
	int i=0;
	adyacencia = malloc(filas * sizeof(int*));
	fichero = fopen("adyacencia.dat","r");        
	
	if (!fichero)
		printf("Error de lectura\n");
	else{
		m = malloc(columnas * sizeof(int));
		while(i != columnas){
			fscanf(fichero,"%d %d %d %d %d %d %d %d", &m[0],&m[1],&m[2],&m[3],&m[4],&m[5],&m[6],&m[7]);
			adyacencia[i] = m;
			i++;	
			if(i != columnas)
				m = malloc(columnas * sizeof(int));
		}
		MostrarDatos(adyacencia, filas, columnas);
	}
	fclose(fichero);
	
	for(i=0; i<filas; i++){
		for(j=0; j<columnas; j++){
			if(adyacencia[i][j] == 0){
				adyacencia[i][j]=999;
			}
		}
	}
	
	printf("\nLos arboles de expansion minimos de expansion y sus costos son:\n");
	while(ne < filas){
		for(i=0,min=999;i<filas;i++){
			for(j=0;j < filas;j++){
				if(adyacencia[i][j] < min){
					min=adyacencia[i][j];
					a=u=i;
					b=v=j;
				}
			}
		}
		u=Busqueda(u);
		v=Busqueda(v);
		if(uni(u,v)){
			printf("%d -> (%d,%d) =%d\n",ne++,a,b,min);
			costomin =  min + costomin;
		}
		adyacencia[a][b]=adyacencia[b][a]=999;
	}
	printf("\nCosto minimo segun Kruskal = %d\n",costomin);
	return 0;
}
