#include <iostream>
#define N 6

int mat[N][N]= {{0,1,1,0,0,0},{1,0,0,1,0,0},{1,0,0,0,0,0},{0,1,0,0,1,1},{0,0,0,1,0,1},{0,0,0,1,1,0}};

int Visitados[N]= {0,0,0,0,0,0};

void DFS(int V){
	Visitados[V] = 1;
	std::cout << V;
	for(int W=0;W<N;W++){ //Se buscan los adyacentes de V
		if (mat[V][W] == 1){ //Se encontró con un adyacente(W)
			if (Visitados[W] == 0){ //Si no ha sido visitado
				DFS(W);
			}
		}
	}
}

int main(){
	int ini = 0;
	DFS(ini);
	return 0;
}
